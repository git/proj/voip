# Mounir Lamouri <volkmar@gentoo.org> (29 Apr 2009)
# openser is masked for removal because the project is dead
# openser SRC_URI are not available anymore
# opensips is the new openser project
# if you are using this package, you may want to look at bug 107482
net-misc/openser

# rambaldi@xs4all.nl (21 dec 2008)
# asterisk 1.2 related ebuilds
# as asterisk < 1.4 is about to be masked and removed in the 
# official portage tree, do the same for the voip overlay... 
# masked packages will be removed when asterisk-1.2 is removed from the 
# official portage tree.
#
# make some noise if I masked to much.....
#
net-misc/asterisk-app_backticks
net-misc/asterisk-app_changrab
net-misc/asterisk-app_conference
net-misc/asterisk-app_contest
net-misc/asterisk-app_distributor
net-misc/asterisk-app_event
net-misc/asterisk-app_iconv
net-misc/asterisk-app_intercept
net-misc/asterisk-app_ldap
net-misc/asterisk-app_notify
net-misc/asterisk-app_rtxfax
net-misc/asterisk-app_valetparking
net-misc/asterisk-cdr_shell
net-misc/asterisk-chan_capi
net-misc/asterisk-chan_misdn
net-misc/asterisk-chan_ss7
net-misc/asterisk-chan_unicall
net-misc/asterisk-chan_visdn
net-misc/asterisk-format_base64
net-misc/asterisk-res_perl
net-misc/asterisk-sounds-au
net-misc/asterisk-sounds-de
net-misc/asterisk-sounds-it
net-misc/asterisk-sounds-se

#new sourceforge releases break ekiga
>=dev-libs/pwlib-1.11
