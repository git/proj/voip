# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="GNU ccAudio"
HOMEPAGE="http://www.gnu.org/software/ccaudio/"
SRC_URI="mirror://gnu/ccaudio/${P}.tar.gz"

KEYWORDS="~x86"
LICENSE="GPL-2"
IUSE=""
SLOT="0"

RDEPEND=">=dev-cpp/commoncpp2-1.3.0"
DEPEND="${RDEPEND}"

src_install() {
	emake DESTDIR=${D} install || die "emake install failed"
	dodoc README ChangeLog AUTHORS NEWS TODO
}
