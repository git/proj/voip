# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=1

inherit eutils

DESCRIPTION="YATE - Yet Another Telephony Engine"
HOMEPAGE="http://yate.null.ro/"
SRC_URI="http://yate.null.ro/tarballs/yate2/yate2.tar.gz"

S=${WORKDIR}/${PN}

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="amrnb doc gsm h323 ilbc mysql postgres qt4 spandsp speex ssl wanpipe zaptel"

DEPEND="media-sound/sox
	amrnb? ( media-libs/amrnb )
	doc? ( || ( app-doc/doxygen >=dev-util/kdoc-2.0_alpha54 ) )
	gsm? ( media-sound/gsm )
	h323? ( >=net-libs/openh323-1.15.3
		dev-libs/pwlib )
	mysql? ( dev-db/mysql )
	postgres? ( dev-db/postgresql-base )
	qt4? ( x11-libs/qt-core:4 x11-libs/qt-gui:4 )
	spandsp? ( media-libs/spandsp )
	speex? ( media-libs/speex )
	ssl? ( dev-libs/openssl )
	wanpipe? ( net-misc/wanpipe )
	zaptel? ( net-misc/zaptel )"
RDEPEND="${DEPEND}"

src_compile() {
	# Figure out which doc generators are available.
	local extraopts
	if use doc && has_version app-doc/doxygen; then
		extraopts+=" --with-doxygen"
	else
		extraopts+=" --without-doxygen"
	fi

	if use doc && has_version dev-util/kdoc; then
		extraopts+=" --with-kdoc"
	else
		extraopts+=" --without-kdoc"
	fi

	econf \
		$(use_enable ilbc) \
		$(use_with amrnb amrnb /usr) \
		$(use_with gsm libgsm) \
		$(use_with h323 openh323 /usr) \
		$(use_with h323 pwlib /usr) \
		$(use_with mysql mysql /usr) \
		$(use_with postgres libpq /usr) \
		$(use_with qt4 libqt4) \
		$(use_with spandsp) \
		$(use_with speex libspeex) \
		$(use_with ssl openssl) \
		$(use_with wanpipe wphwec /usr) \
		${extraopts} \
		|| die "Configure failed"

	emake -j1 all || die "Building failed"
}

src_install() {
	emake -j1 DESTDIR=${D} install-noapi || die "emake install-noapi failed"

	if use doc; then
		emake -j1 DESTDIR=${D} install-api || die "emake install-api failed"
	fi

	newinitd ${FILESDIR}/${PF}/yate.initd yate
	newconfd ${FILESDIR}/${PF}/yate.confd yate

	insinto /usr/share/doc/${PF}/scripts
	cp -R ${S}/share/scripts/* ${D}/usr/share/doc/${PF}/scripts/
	rm -f ${D}/usr/share/doc/${PF}/scripts/Makefile*
}
