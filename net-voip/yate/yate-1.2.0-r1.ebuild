# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="YATE - Yet Another Telephony Engine"
HOMEPAGE="http://yate.null.ro/"
SRC_URI="http://yate.null.ro/tarballs/yate1/${P}-1.tar.gz"

S=${WORKDIR}/${PN}

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc gsm gtk h323 ilbc mysql postgres pri spandsp speex wanpipe zaptel"

DEPEND="media-sound/sox
	doc? ( || ( app-doc/doxygen >=dev-util/kdoc-2.0_alpha54 ) )
	gsm? ( media-sound/gsm )
	gtk? ( >=x11-libs/gtk+-2.6.8 )
	h323? ( >=net-libs/openh323-1.15.3
		dev-libs/pwlib )
	mysql? ( dev-db/mysql )
	postgres? ( dev-db/postgresql )
	pri? ( net-libs/libpri )
	spandsp? ( media-libs/spandsp )
	speex? ( media-libs/speex )
	wanpipe? ( net-misc/wanpipe )
	zaptel? ( net-misc/zaptel )"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}

	# fix gtk2 plugin makefile
	epatch ${FILESDIR}/${PN}-0.9.0-gtk2.diff
	epatch ${FILESDIR}/${PN}-1.2.0-enable-speex-in-ysip.patch
}

src_compile() {
	# Figure out which doc generators are available.
	local extraopts
	if use doc && has_version app-doc/doxygen; then
		extraopts+=" --with-doxygen"
	else
		extraopts+=" --without-doxygen"
	fi

	if use doc && has_version dev-util/kdoc; then
		extraopts+=" --with-kdoc"
	else
		extraopts+=" --without-kdoc"
	fi

	econf \
		$(use_enable ilbc) \
		$(use_with gsm libgsm) \
		$(use_with gtk libgtk2) \
		$(use_with h323 openh323 /usr) \
		$(use_with h323 pwlib /usr) \
		$(use_with mysql mysql /usr) \
		$(use_with postgres libpq /usr) \
		$(use_with pri libpri) \
		$(use_with spandsp) \
		$(use_with speex libspeex) \
		$(use_with wanpipe wphwec /usr) \
		${extraopts} \
		|| die "Configure failed"

	emake all contrib || die "Building failed"

	if use doc; then
		emake apidocs || die "Building API docs failed"
	fi
}

src_install() {
	emake DESTDIR=${D} install-noapi || die "emake install-noapi failed"

	if use doc; then
		emake DESTDIR=${D} install-api || die "emake install-api failed"
	fi

	newinitd ${FILESDIR}/yate.rc6 yate
	newconfd ${FILESDIR}/yate.confd yate

	cd ${D}/usr/share/doc
	mv ${P} ${PF}

	insinto /usr/share/doc/${PF}/scripts
	cp -R ${S}/scripts/* ${D}/usr/share/doc/${PF}/scripts/
	rm -f ${D}/usr/share/doc/${PF}/scripts/Makefile*
}
