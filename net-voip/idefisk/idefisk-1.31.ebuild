# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="SIP and IAX/IAX2 software phone"
HOMEPAGE="http://www.asteriskguru.com/idefisk/"
SRC_URI="http://www.asteriskguru.com/tools/idefisklinux/idefisk131.tar.gz"

LICENSE="as-is"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="x11-libs/libX11
	x11-libs/libXext
	x11-libs/libXrandr
	x11-libs/libXft
	x11-libs/libXi
	x11-libs/libXrender
	x11-libs/libXcursor
	>=x11-libs/gtk+-2
	dev-libs/atk
	x11-libs/pango
	dev-libs/glib
	>=dev-libs/expat-2.0.0"
S=${WORKDIR}

src_install() {
	dodoc CHANGELOG
	exeinto /opt/idefisk
	doexe idefisk libiaxclient.so
	make_wrapper idefisk /opt/idefisk/idefisk /opt/idefisk /opt/idefisk /opt/bin
}
