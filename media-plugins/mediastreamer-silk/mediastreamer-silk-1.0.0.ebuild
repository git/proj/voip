# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-plugins/mediastreamer-silk/mediastreamer-silk-0.0.1.ebuild,v 1.9 2013/06/29 19:23:39 ago Exp $

EAPI=5

MY_PN="mssilk"

inherit autotools eutils

DESCRIPTION="SILK (skype codec) support for Linphone"
HOMEPAGE="http://www.linphone.org"
SRC_URI="mirror://nongnu/linphone/plugins/sources/${MY_PN}-${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~ppc64 ~x86"
IUSE=""

RDEPEND=">=media-libs/mediastreamer-2.8.2"
DEPEND="${RDEPEND}
	>=media-libs/silk-1.0.9[pic]
	virtual/pkgconfig"

S="${WORKDIR}/${MY_PN}-${PV}"

src_prepare() {
	epatch "${FILESDIR}/${P}-system-silk.patch"
	eautoreconf
}
