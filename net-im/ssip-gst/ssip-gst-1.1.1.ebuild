# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

IUSE="ssl"

DESCRIPTION="Sofia-SIP based SIP UA plugin for Gaim"
HOMEPAGE="http://sofia-sip.sourceforge.net/"
SRC_URI="mirror://sourceforge/sofia-sip/sofiasip-gaim-prpl-${PV}.tar.gz"

SLOT="0"
LICENSE="LGPL-2.1"
KEYWORDS="~x86"

RDEPEND="dev-libs/glib
	>=net-libs/sofia-sip-1.11.4
	>=net-im/gaim-1.5.0"

DEPEND="${RDEPEND}
	dev-util/pkgconfig"

RESTRICT="nomirror"

S="${WORKDIR}/sofiasip-gaim-prpl-${PV}"

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}"/${P}-makefile.diff

	# gaim-1.5 api compat
	epatch "${FILESDIR}"/${P}-gaim15_api.diff
}

src_compile() {
	econf \
		--with-gaim-libdir=/usr/$(get_libdir)/gaim || die

	emake || die
}

src_install() {
	make DESTDIR="${D}" install-plugin || die

	dodoc ChangeLog COPYRIGHTS README TODO COPYING AUTHORS
}
