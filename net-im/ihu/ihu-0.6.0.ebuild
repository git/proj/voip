# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils qt3

DESCRIPTION="Peer-to-Peer VoIP Application"
HOMEPAGE="http://ihu.sourceforge.net/"
SRC_URI="mirror://sourceforge/ihu/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="jack"

DEPEND="$(qt_min_version 3.2)
	media-libs/libsoundtouch
	media-libs/speex
	media-libs/libogg
	media-libs/alsa-lib
	jack? ( media-sound/jack-audio-connection-kit )"

src_compile() {
	econf \
		--with-Qt-dir=${QTDIR} \
		`use_enable jack` \
	|| die "configure failed"
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	make_desktop_entry ihu "I Hear U" "/usr/share/doc/ihu/ihubig.png" "Network;Telephony;Qt"
}
