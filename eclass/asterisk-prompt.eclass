# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
# Author: Stefan Briesenick <sbriesen@gentoo.org>

# This eclass is designed to streamline the construction of
# ebuilds for Asterisk voice prompts.

inherit asterisk-mod
EXPORT_FUNCTIONS src_compile src_install

DESCRIPTION="Based on the ${ECLASS} eclass"
DEPEND="net-misc/asterisk"

AST_PROMPT_DOCS=""
AST_PROMPT_EXTS="gsm wav"
AST_PROMPT_LANG="${AST_PN:7:2}"
AST_PROMPT_DIRS="dictate digits letters phonetic"

asterisk-prompt_src_compile() { :; }

asterisk-prompt_src_install() {
	local x z
	for x in ${AST_PROMPT_DIRS}; do
		if [[ -d "${x}" ]]; then
			z="${x}/${AST_PROMPT_LANG}"
			insinto "${AST_SOUNDS_DIR}/${z}"
			if [[ -d "${z}" ]]; then
				doins -r "${z}/."
			else
				doins -r "${x}/."
			fi
		fi
	done
	insinto "${AST_SOUNDS_DIR}/${AST_PROMPT_LANG}"
	if [[ -d "${AST_PROMPT_LANG}" ]]; then
		doins -r "${AST_PROMPT_LANG}/."
	else
		for x in ${AST_PROMPT_EXTS}; do
			doins 2>/dev/null *."${x}"
		done
	fi

	# if no docs specified, check default list
	if [[ -z "${AST_PROMPT_DOCS}" ]]; then
		for x in ${AST_DOCS}; do
			[[ -s "${x}" ]] && AST_PROMPT_DOCS="${AST_PROMPT_DOCS} ${x}"
		done
	fi

	if [[ -n "${AST_PROMPT_DOCS}" ]]; then
		echo ">>> Installing documentation files..."
		dodoc ${AST_PROMPT_DOCS}
	fi

	ast_fix_permissions
}
