# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
# Author: Stefan Briesenick <sbriesen@gentoo.org>

# This eclass is designed to streamline the construction of
# ebuilds for Asterisk modules.
#
# The following variables are all optional and have reasonable
# defaults. You should override them only if you have special
# needs:
#
# AST_MODULES="app_foo app_bar"   (default: ${AST_PN})
#
# AST_${modulename}_SOURCE="app_foo.c bar.c"
# AST_${modulename}_TARGET="app_foo.so"
# AST_${modulename}_LIBS="-ltest -lm"
# AST_${modulename}_CFLAGS="-D_IM_A_FLAG -I./inc"
# AST_${modulename}_LDFLAGS="-Wl,-z,now"
#
# AST_MOD_DOCS="README INSTALL bla"
# AST_MOD_CONF="foo.conf sample/bar.conf"
#
# There are also a couple of variables which are set by this eclass
# and shouldn't be set by hand. These are as follows:
#
# Env Var          Option     Description
# AST_PN           <string>   Ebuild ${PN} w/o leading "asterisk-"
# AST_P            <string>   dito for ${P}
# AST_VERSION      <string>   The Asterisk version. ie: 1.2.12.1-BRIstuffed-0.3.0-PRE-1s
# AV_MAJOR         <integer>  The Asterisk major version. ie: 1
# AV_MINOR         <integer>  The Asterisk minor version. ie: 2
# AV_PATCH         <integer>  The Asterisk patch version. ie: 12.1
# AV_EXTRA         <string>   The Asterisk extra version. ie: -BRIstuffed-0.3.0-PRE-1s
# AV_VER_X         <string>   The major/minor version + 'x'. ie: 1.2.x
# AST_AGI_DIR      <string>   AGI files belong there (ie: /var/lib/asterisk/agi-bin)
# AST_LIB_DIR      <string>   Libraries are put there (ie: /usr/lib/asterisk)
# AST_LOG_DIR      <string>   The place for logfiles (ie: /var/log/asterisk)
# AST_SPOOL_DIR    <string>   Asterisk's spool directory (ie: /var/spool/asterisk)
# AST_VAR_LIB_DIR  <string>   Asterisk's /var/lib directory (ie: /var/lib/asterisk)
# AST_VAR_RUN_DIR  <string>   Asterisk's /var/run directory (ie: /var/run/asterisk)
# AST_INCLUDE_DIR  <string>   Include files (ie: /usr/include/asterisk)
# AST_MODULES_DIR  <string>   Asterisk keeps its modules here (ie: /usr/lib/asterisk/modules)
# AST_CONFIG_DIR   <string>   The place for config files (ie: /etc/asterisk)
# AST_SOUNDS_DIR   <string>   The place for voice prompts (ie: /var/lib/asterisk/sounds)


inherit eutils toolchain-funcs versionator
EXPORT_FUNCTIONS pkg_setup src_compile src_install

IUSE=""
SLOT="0"
DESCRIPTION="Based on the ${ECLASS} eclass"

AST_DOCS="AUTHORS CHANGES ChangeLog INSTALL NEWS README TODO"
AST_PN="${PN/asterisk-}"
AST_P="${AST_PN}-${PV}"

AST_MOD_DOCS=""
AST_MOD_CONF=""

S="${WORKDIR}/${AST_P}"

# Sometimes we have to fix "#include" headers. You can
# use this helper-function:
#
ast_fix_source() {  # [-e "further expressions" ...] <files...>
	sed -i -e 's:^\(#include.*\)[<"]\(asterisk/.*\)[">]:\1<\2>:g' \
		-e 's:^\(#include.*\)[<"]\(asterisk\.h\)[">]:\1<asterisk/\2>:g' \
		"${@}"
}

# If your ebuild installs something into ${AST_CONFIG_DIR}, ${AST_LOG_DIR},
# ${AST_SPOOL_DIR}, ${AST_VAR_LIB_DIR} or ${AST_VAR_RUN_DIR}, you should
# run this helper-function somewhere in src_install().
#
ast_fix_permissions() {
	local DIR
	echo ">>> Fixing permissions..."
	if [[ -n "$(egetent group asterisk)" ]]; then
		for DIR in "${AST_CONFIG_DIR}"; do
			if [[ -d "${D}${DIR}" ]]; then
				fowners -R root:asterisk "${DIR}"
				fperms  -R u=rwX,g=rX,o= "${DIR}"
			fi
		done
		if [[ -n "$(egetent passwd asterisk)" ]]; then
			for DIR in "${AST_LOG_DIR}" "${AST_SPOOL_DIR}" \
				"${AST_VAR_LIB_DIR}" "${AST_VAR_RUN_DIR}"; do
				if [[ -d "${D}${DIR}" ]]; then
					fowners -R asterisk:asterisk "${DIR}"
					fperms  -R u=rwX,g=rX,o= "${DIR}"
				fi
			done
		fi
	fi
}

# drop-in replacements für *into and do/new*
#
ast_domod() {
	exeinto "${AST_MODULES_DIR}"
	doexe "${@}"
}

ast_newmod() {
	exeinto "${AST_MODULES_DIR}"
	newexe "${@}"
}

ast_doconf() {
	insinto "${AST_CONFIG_DIR}"
	doins "${@}"
	dodoc "${@}" 
}

ast_newconf() {
	insinto "${AST_CONFIG_DIR}"
	newins "${@}"
	newdoc "${@}"
}

# echo + exec ;-)
#
ast_do_compile() {
	echo "${@}" && "${@}"
}

# call this function as first command if you overide
# pkg_setup() in your ebuild:
#
asterisk-mod_pkg_setup() {
	local astconf="${ROOT}usr/bin/asterisk-config"
	export AST_VERSION="$($astconf --version)"

	export AV_MAJOR=$(get_version_component_range 1 ${AST_VERSION})
	export AV_MINOR=$(get_version_component_range 2 ${AST_VERSION})
	export AV_PATCH=$(get_version_component_range 3- ${AST_VERSION})
	export AV_PATCH=${AV_PATCH//-*}
	[[ -n ${AST_VERSION#*-} ]] \
		&& [[ -n ${AST_VERSION//${AST_VERSION#*-}} ]] \
		&& AV_EXTRA="-${AST_VERSION#*-}"
	export AV_VER_X="${AV_MAJOR}.${AV_MINOR}.x"

	einfo "Asterisk version: ${AST_VERSION}"

	export AST_AGI_DIR="$($astconf --agidir)"
	export AST_LIB_DIR="$($astconf --libdir)"
	export AST_LOG_DIR="$($astconf --logdir)"
	export AST_SPOOL_DIR="$($astconf --spooldir)"
	export AST_VAR_LIB_DIR="$($astconf --varlibdir)"
	export AST_VAR_RUN_DIR="$($astconf --varrundir)"
	export AST_INCLUDE_DIR="$($astconf --includedir)"
	export AST_MODULES_DIR="$($astconf --modulesdir)"
	export AST_CONFIG_DIR="$($astconf --sysconfdir)"
	export AST_SOUNDS_DIR="${AST_VAR_LIB_DIR}/sounds"

	export AST_CFLAGS="-g -Wall -D_REENTRANT -D_GNU_SOURCE -fPIC"
	export AST_LDFLAGS="$($astconf --solink)"
	export AST_CC="$(tc-getCC)"
}

# default src_compile(). Doesn't need any Makefiles, just define
# the variables mentioned above as needed and you're done.
#
asterisk-mod_src_compile() {
	local mod_objs mod_libs mod_docs mod_source mod_target
	local mod_cflags mod_ldflags modver currm obj src mod

	for mod in ${AST_MODULES:=${AST_PN}}; do

		echo ">>> Building ${mod}..."

		currm=$(echo ${mod//-/_} | tr '[:lower:]' '[:upper:]')

		mod_objs=""
		mod_libs="$(eval echo \${AST_${currm}_LIBS})"
		mod_docs="$(eval echo \${AST_${currm}_DOCS})"
		mod_source="$(eval echo \${AST_${currm}_SOURCE})"
		mod_target="$(eval echo \${AST_${currm}_TARGET})"
		mod_cflags="$(eval echo \${AST_${currm}_CFLAGS})"
		mod_ldflags="$(eval echo \${AST_${currm}_LDFLAGS})"

		[[ -z "${mod_source}" ]] && mod_source="${mod}.c"
		[[ -z "${mod_target}" ]] && mod_target="${mod}.so"

		# add some nice version information to the final module
		modver="_${mod}_version"
		cat > "${modver}.c" <<-EOF
			const char __${mod}_${AV_VER_X//./_}[0];
		EOF
		mod_source="${mod_source} ${modver}.c"

		# compile source files
		for src in ${mod_source}; do
			obj=$(echo "${src}" | sed -e "s:^\(.*\)\..*:\1.o:")
			ast_do_compile ${AST_CC} ${AST_CFLAGS} ${CFLAGS} ${mod_cflags} \
				-c -o ${obj} ${src} || die "Compiling ${x} failed!"
			mod_objs="${mod_objs} ${obj}"
		done

		# link module
		if [[ -n "${mod_objs}" ]]; then
			ast_do_compile $(tc-getCC) \
				${AST_LDFLAGS} ${LDFLAGS} ${mod_ldflags} \
				-o ${mod_target} ${mod_objs} ${mod_libs} \
			|| die "Linking ${MOD_TARGET} failed!"
		fi
	done
}

# default src_install()
#
asterisk-mod_src_install() {
	local mod doc cnf currm mod_target

	exeinto "${AST_MODULES_DIR}"
	for mod in ${AST_MODULES:=${AST_PN}}; do
		echo ">>> Installing ${mod}..."
		currm=$(echo ${mod//-/_} | tr '[:lower:]' '[:upper:]')
		mod_target="$(eval echo \${AST_${currm}_TARGET})"
		[[ -z "${mod_target}" ]] && mod_target="${mod}.so"
		doexe ${mod_target} || die "Failed to install ${mod}.so!"
	done

	# if no docs specified, check default list
	if [[ -z "${AST_MOD_DOCS}" ]]; then
		for doc in ${AST_DOCS}; do
			[[ -s "${doc}" ]] && AST_MOD_DOCS="${AST_MOD_DOCS} ${doc}"
		done
	fi

	# if no conf specified, check default list
	if [[ -z "${AST_MOD_CONF}" ]]; then
		for cnf in "${AST_PN}" "${AST_PN/app_}" "${AST_PN/cdr_}" \
			"${AST_PN/chan_}" "${AST_PN/codec_}" "${AST_PN/format_}" \
			"${AST_PN/func_}" "${AST_PN/pbx_}" "${AST_PN/res_}"; do
			[[ -s "${cnf}.conf" ]] && AST_MOD_CONF="${AST_MOD_CONF} ${cnf}.conf"
		done
	fi

	if [[ -n "${AST_MOD_DOCS}" ]]; then
		echo ">>> Installing documentation files..."
		dodoc ${AST_MOD_DOCS}
	fi

	if [[ -n "${AST_MOD_CONF}" ]]; then
		echo ">>> Installing configuration files..."
		ast_doconf ${AST_MOD_CONF}
	fi

	ast_fix_permissions
}
