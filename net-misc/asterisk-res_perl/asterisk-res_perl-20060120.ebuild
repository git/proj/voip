# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

MY_PN="res_perl"

DESCRIPTION="Asterisk ... JavaScript"
HOMEPAGE="http://www.pbxfreeware.org/"
SRC_URI="http://www.netdomination.org/pub/asterisk/${P}.tgz
	mirror://gentoo/${P}.tgz"

IUSE=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"

RDEPEND="dev-lang/perl
	>=net-misc/asterisk-1.2.0
	!<net-misc/asterisk-1.1.0"

DEPEND="${RDEPEND}
	dev-lang/swig"

S=${WORKDIR}/${MY_PN}

pkg_setup() {
	einfo "Running pre-flight check..."
	if ! built_with_use "dev-lang/perl" ithreads; then
		eerror "${PF} requires a version of dev-lang/perl and sys-devel/libperl"
		eerror "that has been built with the \"ithreads\" use-flag enabled"
		eerror "Please remerge sys-devel/libperl and dev-lang/perl!"
		die "Required conditions not met"
	fi
}

src_unpack() {
	unpack ${A}

	cd ${S}
	# use asterisk-config...
	epatch ${FILESDIR}/${MY_PN}-20060120-gentoo.diff

	# asterisk.h -> asterisk/asterisk.h
	sed -i  -e "s:\"asterisk\.h\":<asterisk/asterisk.h>:" \
		-e "s:<asterisk\.h>:<asterisk/asterisk.h>:" \
		res_perl.c res_perl.h

	if built_with_use "net-misc/asterisk" bri; then
		epatch ${FILESDIR}/${MY_PN}-${PV}-bristuff.diff
	fi
}

src_compile() {
	emake -j1 \
		PREFIX=/var/lib/asterisk || die "emake failed"
}

src_install() {
	make \
		DESTDIR=${D} \
		PREFIX=/var/lib/asterisk install || die "installation failed"

	dodoc README README.debian LICENSE INSTALL

	#fix permissions
	if [[ -n "$(egetent group asterisk)" ]] && \
	   [[ -n "$(egetent passwd asterisk)" ]]; then	
		chown -R asterisk:asterisk ${D}var/lib/asterisk
		chmod -R u=rwX,g=rX,o=     ${D}var/lib/asterisk
	fi
}

