# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-mod subversion

DESCRIPTION="Asterisk application plugin for conferences"
HOMEPAGE="http://iaxclient.sourceforge.net/"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~ppc ~x86"

ESVN_REPO_URI="https://svn.sourceforge.net/svnroot/iaxclient/trunk/${AST_PN}"
ESVN_OPTIONS="-r {${PV}}"

# depends on glibc's iconv support
DEPEND="sys-libs/glibc
	>=net-misc/asterisk-1.2.0"

AST_APP_CONFERENCE_CFLAGS="-DCRYPTO -DSILDET=2 -Ilibspeex"
AST_APP_CONFERENCE_SOURCE="app_conference.c conference.c member.c frame.c \
	cli.c libspeex/preprocess.c libspeex/misc.c libspeex/smallft.c"
