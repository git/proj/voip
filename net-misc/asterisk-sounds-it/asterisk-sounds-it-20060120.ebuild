# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-prompt

MY_PN="it_mm_sounds"

DESCRIPTION="Italian sounds for Asterisk"
HOMEPAGE="http://mirror.tomato.it/ftp/pub/asterisk/suoni_ita/"
SRC_URI="http://mirror.tomato.it/ftp/pub/asterisk/suoni_ita/${MY_PN}_${PV}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~alpha ~amd64 ~hppa ~ppc ~sparc ~x86"

S="${WORKDIR}"

AST_PROMPT_DOCS="readme.pdf"
