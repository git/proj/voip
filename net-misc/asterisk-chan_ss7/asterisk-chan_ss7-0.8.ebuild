# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-misc/asterisk-app_notify/asterisk-app_notify-0.9.ebuild,v 1.1 2005/08/27 21:50:26 stkn Exp $

inherit eutils

MY_PN="chan_ss7"

DESCRIPTION="Asterisk SS7 channel plugin"
HOMEPAGE="http://www.sifira.com/chan-ss7/"
SRC_URI="http://www.sifira.com/chan-ss7/${PV}/${MY_PN}-${PV}.tar.gz"

IUSE=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"

RDEPEND="
	>=net-misc/zaptel-1.2.0
	>=net-misc/asterisk-1.2.0"

DEPEND="${RDEPEND}"

S=${WORKDIR}

src_unpack() {
	unpack ${A}

	cd ${S}
	# tweak makefile
	epatch ${FILESDIR}/${MY_PN}-0.8-gentoo.diff
}

src_compile() {
	emake -j1 \
		INCLUDE=-I/usr/include/zaptel || die "emake failed"
}

src_install() {
	make INSTALL_PREFIX=${D} install || die

	dodoc NEWS README ANNOUNCE COPYING INSTALL

	insinto /etc/asterisk
	doins ${FILESDIR}/ss7.conf.*

	if [[ -n "$(egetent group asterisk)" ]]; then
		chown -R root:asterisk ${D}/etc/asterisk
		chmod -R u=rwX,g=rX,o= ${D}/etc/asterisk
	fi
}

pkg_postinst() {
	ewarn "This is ALPHA qualitiy software"
	ewarn " !! Use at your own risk !! "
	echo
	einfo "Useful resources:"
	echo
	einfo "  http://www.voip-info.org/wiki/index.php?page=Asterisk+ss7+channels"

	echo
	ewarn "Please re-create your configuration using the new templates in ${ROOT}/etc/asterisk/ss7.conf.template*"
}
