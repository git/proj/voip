# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-prompt

MY_PN="asterisk-prompt-se"

DESCRIPTION="Swedish sounds for Asterisk"
HOMEPAGE="http://www.danielnylander.se/asterisk-saker/"
SRC_URI="http://www.danielnylander.se/asterisk/${MY_PN}_${PV}-orig.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~alpha ~amd64 ~hppa ~ppc ~sparc ~x86"
RESTRICT="nomirror"

S="${WORKDIR}/sounds"

AST_PROMPT_DIRS="${AST_PROMPT_DIRS} areacodes ha wx"

src_unpack() {
	unpack ${A}
	cd "${S}"

	# minor cleanups
	cp -f voicemail-CVS-fix/*.gsm se/
	rm -f phonetic/se/README
}
