# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-prompt

MY_P="ast_prompts_de_v${PV//./_}"

DESCRIPTION="German sounds for Asterisk"
HOMEPAGE="http://www.beronet.com/index.php?option=com_content&task=view&id=50&Itemid=1&lang=en"
SRC_URI="http://www.beronet.com/downloads/${MY_P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~alpha ~amd64 ~hppa ~ppc ~sparc ~x86"
RESTRICT="nomirror"

S="${WORKDIR}/${MY_P}/var/lib/asterisk/sounds"
