# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-mod

DESCRIPTION="Asterisk application plugin to notify users"
HOMEPAGE="http://www.mezzo.net/asterisk/"
SRC_URI="http://www.mezzo.net/asterisk/${AST_P}.tgz"

LICENSE="GPL-2"
KEYWORDS="~x86"

# depends on iconv support
DEPEND="sys-libs/glibc
	>=net-misc/asterisk-1.0.7-r1"

src_unpack() {
	unpack ${A}
	cd "${S}"

	# patch makefile
	sed -i -e "s:^\(RES=\).*echo \"\(.*\)\".*:\1\2:g" \
		-e "s:^\(CFLAGS\)=:\1+=:g" Makefile
}

src_compile() {
	emake CC=$(tc-getCC) SOLINK="${AST_LDFLAGS} ${LDFLAGS}" || die "emake failed"
}

src_install() {
	asterisk-mod_src_install

	# sample dialer config (not needed for app_notify)
	newdoc dialer_extensions.conf.sample dialer_extensions.conf

	# install samples 
	insinto /usr/share/doc/${PF}/examples
	doins sample*.c
}
