# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-mod

DESCRIPTION="Asterisk applications for sending and receiving faxes"
HOMEPAGE="http://www.soft-switch.org/"
SRC_URI="http://www.netdomination.org/pub/asterisk/${P}.tar.bz2"

RESTRICT="nomirror"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"

DEPEND=">=media-libs/spandsp-0.0.2_pre20
	>=net-misc/asterisk-1.0.5-r1"

S="${WORKDIR}/${AST_PN}-${PV}"

AST_MODULES="app_rxfax app_txfax"
AST_APP_RXFAX_LIBS="-lspandsp -ltiff"
AST_APP_TXFAX_LIBS="-lspandsp -ltiff"

src_unpack() {
	unpack ${A}
	cd "${S}"

	# apply spandsp-0.0.3 patch
	if has_version "=media-libs/spandsp-0.0.3*"; then
		epatch "${FILESDIR}/${AST_PN}-spandsp-0.0.3.diff"
	fi

	# fix includes
	ast_fix_source asterisk*/*.c

	# link correct source for installed asterisk version
	ln -snf asterisk-${AV_VER_X}/*.c .
}
