# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils linux-mod toolchain-funcs

DESCRIPTION="Linux Voice TDM/WAN Router Package"
HOMEPAGE="http://www.sangoma.com/"
SRC_URI="ftp://ftp.sangoma.com/linux/current_wanpipe/${P//_beta/}.tgz"

LICENSE="WANPIPE"

SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="
	sys-libs/ncurses"

DEPEND="
	sys-devel/flex
	>=virtual/linux-sources-2.6
	${RDEPEND}"

S="${WORKDIR}/${P//_beta/}"
S_BUILD="${WORKDIR}/build-tmp"

pkg_setup() {
	linux-mod_pkg_setup
}

src_unpack() {
	unpack ${A}

	# Make a minimal replica of the current system linux kernel headers.
	mkdir -p ${S_BUILD}
	cd ${S_BUILD}
	cp -a ${KV_DIR}/include ${S_BUILD}
	ln -s ${KV_DIR}/Makefile
	ln -s ${KV_DIR}/arch
	ln -s ${KV_DIR}/scripts

	cd ${S}
	epatch ${FILESDIR}/${P//_beta/}-setup.diff
	epatch ${FILESDIR}/${P//_beta/}-makefile.diff
	epatch ${FILESDIR}/${P//_beta/}-nostrip.diff
	epatch ${FILESDIR}/${P//_beta/}-wancfgzaptel-relocation.diff
}

src_compile() {
	cd ${S}

	einfo "Building kernel modules ..."
	./Setup drivers --silent --builddir=${S_BUILD} --with-linux=${S_BUILD} --usr-cc=$(tc-getCC)

	einfo "Building utilities ..."
	./Setup utility --silent --builddir=${S_BUILD} --with-linux=${S_BUILD} --usr-cc=$(tc-getCC)

	einfo "Building config ..."
	./Setup meta --silent --builddir=${S_BUILD} --with-linux=${S_BUILD} --usr-cc=$(tc-getCC)
}

src_install() {
	cd ${S}

	# Move kernelmodules and includes to staging dir.
	mv ${S_BUILD}/lib ${D}
	mv ${S_BUILD}/usr ${D}

	# Stage the rest of the files.
	mkdir -p ${D}/dev
	./Setup inst --silent --builddir=${D}

	# Fix wanpipe doc dir name.
	mv ${D}/usr/share/doc/wanpipe ${D}/usr/share/doc/${P}

	newinitd ${FILESDIR}/wanpipe.rc6 wanpipe
}
