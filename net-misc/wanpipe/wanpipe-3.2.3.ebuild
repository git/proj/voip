# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils linux-mod mount-boot

DESCRIPTION="Linux Voice TDM/WAN Router Package"
HOMEPAGE="http://www.sangoma.com/"
SRC_URI="ftp://ftp.sangoma.com/linux/current_wanpipe/${P}.tgz"

LICENSE="WANPIPE"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

COMMONDEPEND="sys-libs/ncurses
	virtual/linux-sources"
DEPEND="sys-devel/flex
	>=virtual/linux-sources-2.6
	${COMMONDEPEND}"
RDEPEND="${COMMONDEPEND}"

S_BUILD="${WORKDIR}/build"

pkg_setup() {
	linux-mod_pkg_setup
}

pkg_postinst() {
	mount-boot_pkg_preinst
	depmod -b ${ROOT} -ae -F /boot/System.map-genkernel-${ARCH}-${KV_FULL} ${KV_FULL}
	linux-mod_pkg_postinst
}

src_unpack() {
	unpack ${A}

	# Make a minimal replica of the current system linux kernel headers.
	einfo "Preparing build directory ..."
	mkdir -p ${S_BUILD}
	cd ${S_BUILD}
	cp -a ${KV_DIR}/include ${S_BUILD}
	ln -s ${KV_DIR}/Makefile
	ln -s ${KV_DIR}/arch
	ln -s ${KV_DIR}/scripts

	# Attempt to make the wanpipe build system sane.
	einfo "Attempting to make the wanpipe build system saner ..."
	cd ${S}
	epatch ${FILESDIR}/wanpipe-3.2.3-no-superuser.diff
	epatch ${FILESDIR}/wanpipe-3.2.3-actually-use-user-cflags.diff
	epatch ${FILESDIR}/wanpipe-3.2.3-honor-root-var.diff
	epatch ${FILESDIR}/wanpipe-3.1.0-nostrip.diff
}

src_compile() {
	cd ${S}

	mkdir -p ${S_BUILD}

	einfo "Building kernel modules ..."
	USER_CFLAGS="${CFLAGS}" ./Setup drivers --silent --builddir=${S_BUILD} --with-linux=${S_BUILD} --usr-cc=$(tc-getCC) || die "Failed building kernel modules"

	einfo "Building utilities ..."
	USER_CFLAGS="${CFLAGS}" ./Setup utility --silent --builddir=${S_BUILD} --with-linux=${S_BUILD} --usr-cc=$(tc-getCC) || die "Failed building utilities"

	einfo "Building config ..."
	USER_CFLAGS="${CFLAGS}" ./Setup meta --silent --builddir=${S_BUILD} --with-linux=${S_BUILD} --usr-cc=$(tc-getCC) || die "Failed building config"
}

src_install() {
	cd ${S}

	# Move kernelmodules and includes to staging dir.
	mv ${S_BUILD}/lib ${D}
	mv ${S_BUILD}/usr ${D}

	# Stage the rest of the files.
	mkdir -p ${D}/dev
	./Setup inst --silent --builddir=${D}

	# Remove device files as udev should take care of this.
	rm -rf ${D}/dev

	# Fix wanpipe doc dir name.
	mv ${D}/usr/share/doc/wanpipe ${D}/usr/share/doc/${P}

	# Stage init.d-script.
	newinitd ${FILESDIR}/wanpipe.rc6 wanpipe
}
