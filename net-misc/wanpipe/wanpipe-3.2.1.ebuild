# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils linux-mod mount-boot

DESCRIPTION="Linux Voice TDM/WAN Router Package"
HOMEPAGE="http://www.sangoma.com/"
SRC_URI="ftp://ftp.sangoma.com/linux/current_wanpipe/${P//_beta/}.tgz"

LICENSE="WANPIPE"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

COMMONDEPEND="sys-libs/ncurses
	virtual/linux-sources"
DEPEND="sys-devel/flex
	>=virtual/linux-sources-2.6
	${COMMONDEPEND}"
RDEPEND="${COMMONDEPEND}"

# S="${WORKDIR}/${P//_beta/}"
S_BUILD="${WORKDIR}/build-tmp"

pkg_setup() {
	linux-mod_pkg_setup
}

pkg_postinst() {
	mount-boot_pkg_preinst
	depmod -b ${ROOT} -ae -F /boot/System.map-genkernel-${ARCH}-${KV_FULL} ${KV_FULL}
	linux-mod_pkg_postinst
}

src_unpack() {
	unpack ${A}

	# Make a minimal replica of the current system linux kernel headers.
	mkdir -p ${S_BUILD}
	cd ${S_BUILD}
	cp -a ${KV_DIR}/include ${S_BUILD}
	ln -s ${KV_DIR}/Makefile
	ln -s ${KV_DIR}/arch
	ln -s ${KV_DIR}/scripts

	cd ${S}
	epatch ${FILESDIR}/${PN}-3.1.0-setup.diff
	epatch ${FILESDIR}/${PN}-3.1.0-makefile.diff
	epatch ${FILESDIR}/${PN}-3.1.0-nostrip.diff
# 	epatch ${FILESDIR}/${PN}-3.1.0-wancfgzaptel-relocation.diff
}

src_compile() {
	cd ${S}

	einfo "Building kernel modules ..."
	./Setup drivers --silent --builddir=${S_BUILD} --with-linux=${S_BUILD} --usr-cc=$(tc-getCC)

	einfo "Building utilities ..."
	./Setup utility --silent --builddir=${S_BUILD} --with-linux=${S_BUILD} --usr-cc=$(tc-getCC)

	einfo "Building config ..."
	./Setup meta --silent --builddir=${S_BUILD} --with-linux=${S_BUILD} --usr-cc=$(tc-getCC)

	einfo "Building firmware utilities ..."
	cd util/wan_aftup/
	make wan_aftup || die "Failed building wan_aftup"
}

src_install() {
	cd ${S}

	# Move kernelmodules and includes to staging dir.
	mv ${S_BUILD}/lib ${D}
	mv ${S_BUILD}/usr ${D}

	# Stage the rest of the files.
	mkdir -p ${D}/dev
	./Setup inst --silent --builddir=${D}

	# Stage firmware utils.
	mkdir -p ${D}/etc/wanpipe/firmware
	cp -a ${S}/util/wan_aftup ${D}/etc/wanpipe/firmware
	rm -rf ${D}/etc/wanpipe/firmware/wan_aftup/{*.c,*.h,*.o,Makefile}

	# Remove device files as udev should take care of this.
	rm -rf ${D}/dev

	# Fix wanpipe doc dir name.
	mv ${D}/usr/share/doc/wanpipe ${D}/usr/share/doc/${P}

	newinitd ${FILESDIR}/wanpipe.rc6 wanpipe
}
