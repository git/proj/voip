# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-mod

DESCRIPTION="Asterisk plugin to set up a contest where caller X wins"
HOMEPAGE="http://www.pbxfreeware.org/"
SRC_URI="http://r3mix.eu/pub/dist/asterisk/${P}.tar.bz2"

LICENSE="GPL-2"
KEYWORDS="~x86"

DEPEND=">=net-misc/asterisk-1.2.0"

S="${WORKDIR}/${AST_PN}"
