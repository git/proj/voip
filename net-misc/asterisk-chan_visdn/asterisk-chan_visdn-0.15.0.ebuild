# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils libtool linux-mod

MY_P="visdn-${PV}"

DESCRIPTION=""
HOMEPAGE="http://www.visdn.org/"
SRC_URI="http://www.visdn.org/download/${MY_P}.tar.bz2"


LICENSE="GPL-2"
SLOT="0"

KEYWORDS="~x86"
IUSE="ppp"

RDEPEND=">=net-misc/asterisk-1.0.5-r2"

DEPEND="${RDEPEND}"

S=${WORKDIR}/${MY_P}

src_unpack() {
	unpack ${A}
	cd ${S}

	if [[ ${KV_PATCH} -ge 17 ]]; then
		# fix for 2.6.17
		epatch ${FILESDIR}/${MY_P}-linux-2.6.17.patch
	fi

}

src_compile() {
	econf \
		--with-kernel-build=/usr/src/linux \
		`use_with ppp pppd` || die "econf failed"

	emake ARCH=$(tc-arch-kernel) || die "emake failed"
}

src_install() {
	emake -j1 \
		DESTDIR=${D} \
		ARCH=$(tc-arch-kernel) \
		INSTALL_MOD_PATH=${D} \
		install || die "make install failed"

	dodoc AUTHOR README ChangeLog INSTALL TODO COPYING NEWS

	docinto samples
	dodoc samples/*

	insinto /etc/asterisk
	doins samples/visdn.conf.sample

	# remove unneeded files
	rm ${D}/usr/lib/asterisk/modules/*.a
	rm ${D}/usr/lib/asterisk/modules/*.la

	# workaround, need to investigate this further
	rm -rf ${D}/lib/modules/${KV_FULL}/extra/var

	if [[ -n "$(egetent group asterisk)" ]]; then
		chown -R root:asterisk ${D}/etc/asterisk
		chmod -R u=rwX,g=rX,o= ${D}/etc/asterisk
	fi
}
