# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-prompt

MY_PN="OpenVoice-Free"

DESCRIPTION="Australian sounds for Asterisk"
HOMEPAGE="http://www.openvoice.com.au/"
SRC_URI="http://www.openvoice.com.au/${MY_PN}-${PV}.tar.gz"

LICENSE="CCPL-Attribution-ShareAlike-2.1-Australia"
KEYWORDS="~alpha ~amd64 ~hppa ~ppc ~sparc ~x86"
RESTRICT="nomirror"

S="${WORKDIR}/${MY_PN}"

AST_PROMPT_DOCS="HISTORY.txt prompts.txt README.txt"
