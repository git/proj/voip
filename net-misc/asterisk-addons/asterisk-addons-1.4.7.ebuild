# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

WANT_AUTOCONF="latest"
WANT_AUTOMAKE="latest"

inherit eutils flag-o-matic

IUSE="elibc_uclibc mysql mysql_loguniqueid h323 mp3 debug"

MY_P="${P/_/-}"

DESCRIPTION="Additional Plugins for Asterisk"
HOMEPAGE="http://www.asterisk.org/"
SRC_URI="http://downloads.digium.com/pub/asterisk/${MY_P}.tar.gz"

S=${WORKDIR}/${MY_P}

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"

DEPEND=">=net-misc/asterisk-1.4.0
	mysql? ( virtual/mysql )"
RDEPEND="${DEPEND}"


get_available_modules() {
	local modules mod x

	# build list of available modules...
	for x in app cdr codec format func pbx res chan; do

		for mod in $(find "${S}" -type f -name "${x}_*.c*" -print)
		do
			modules="${modules} $(basename ${mod/%.c*})"
		done
	done

	echo "${modules}"
}

pkg_setup() {
	local n dosleep=0
	einfo "Running pre-flight checks..."

	if use mysql_loguniqueid && ! use mysql; then
		die "The use flag mysql_loguniqueid requires the mysql use flag!";
	fi

	if use h323 && built_with_use net-misc/asterisk h323; then
		echo
		ewarn "h323: Emerging \"${PN}\" with the h323 flag enabled will overwrite asterisk's chan_h323.so!"
		ewarn "h323: Be sure to upgrade \"${ROOT}\"etc/asterisk/h323.conf afterwards!"
		dosleep=1
	fi

	echo
	if [[ $dosleep -gt 0 ]]; then
		ebeep 10
	fi
}

src_unpack() {
	unpack ${A}
	cd "${S}"

	econf
}

src_compile() {
	local modules

	append-flags -fPIC
	use mysql_loguniqueid && append-flags "-DMYSQL_LOGUNIQUEID"

	# build MODULE_LIST
	#
	if ! use mysql; then
		modules="${modules} app_addon_sql_mysql cdr_mysql cdr_addon_mysql  res_mysql res_config_mysql"
	fi

	if ! use mp3 ; then
		modules="${modules} format_mp3"
	fi

	if ! use h323 ; then
		modules="${modules} chan_ooh323"
	fi

	einfo "available modules $(get_available_modules)"
	einfo " skipping modules ${modules}"

	# run menuselect to evaluate the list of modules
	# and rewrite the list afterwards
	#
	if [[ -n "${modules}" ]]
	then
		local mod category tmp_list failed_list

		###
		# run menuselect

		emake menuselect.makeopts || die "emake menuselect.makeopts failed"

		###
		# get list of modules with failed dependencies

		failed_list="$(awk -F= '/^MENUSELECT_DEPSFAILED=/{ print $3 }' menuselect.makeopts)"

		###
		# traverse our list of modules
		einfo "Building menuselect.makeopts..."

		for category in app cdr codec format func pbx res chan; do
			tmp_list=""

			# search list of modules for matching ones first...
			for mod in ${modules}; do
				# module is from current category?
				if [[ "${mod/%_*}" = "${category}" ]]
				then
					# check menuselect thinks the dependencies are met
					if has ${mod} ${failed_list}
					then
						eerror "${mod}: dependencies required to build this module are not met, NOT BUILDING!"
					else
						tmp_list="${tmp_list} ${mod}"
					fi
				fi
			done

			use debug && echo "${category} tmp: ${tmp_list}"

			# replace the module list for $category with our custom one
			if [[ -n "${tmp_list}" ]]
			then
				category="$(echo ${category} | tr '[:lower:]' '[:upper:]')"
				sed -i -e "s:^\(MENUSELECT_${category}.*=\):\1 ${tmp_list}:" \
					menuselect.makeopts || die "failed to set list of ${category} applications"
			fi
		done
	fi

	emake -j1 OPTIMIZE="${CFLAGS}" || die "Make failed"
}

src_install() {
	mkdir -p "${D}"/usr/lib/asterisk/modules
	emake DESTDIR="${D}" install || die "Make install failed"

	# install standard docs...
	dodoc README
	dodoc doc/cdr_mysql.txt

	insinto /usr/share/doc/${PF}
	doins configs/*.sample

	cd "${S}"

	if use h323; then
		insinto /etc/asterisk
		newins configs/ooh323.conf.sample ooh323.conf
	fi

	if use mysql; then
		insinto /etc/asterisk
		newins configs/cdr_mysql.conf.sample cdr_mysql.conf
		newins configs/res_mysql.conf.sample res_mysql.conf
	fi

	einfo "Fixing permissions"
	fowners -R root:asterisk /etc/asterisk
	fperms  -R u=rwX,g=rX,o= /etc/asterisk
}

pkg_postinst() {
	#
	# Announcements, warnings, reminders...
	#
	einfo "********* Some notes from the asterisk-addons-1.4.6 readme: **********"
	echo
	ewarn "\"Using res_config_mysql at the same time as res_config_odbc can create"
	ewarn "system instability on some systems.  Please load only one or the other.\""
	echo
	ewarn "\"format_mp3 can cause Asterisk to crash on certain mp3 files (notably"
	ewarn "8k files made with lame) due to bugs in mpglib.  If you must use this"
	ewarn "module, use it only with mp3's you know will work with it.\""
}
