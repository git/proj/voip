# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-mod

DESCRIPTION="Asterisk plugin to use an external shell script for cdr handling"
HOMEPAGE="http://www.pbxfreeware.org/"
SRC_URI="http://r3mix.eu/pub/dist/asterisk/${P}.tar.bz2"

LICENSE="GPL-2"
KEYWORDS="~ppc ~x86"

DEPEND=">=net-misc/asterisk-1.2.0"

S="${WORKDIR}/${AST_PN}"

AST_MOD_CONF="cdr.conf"

src_unpack() {
	unpack ${A}
	cd "${S}"

	ast_fix_source \
		-e "s:%d\(.*cdr->duration\):%ld\1:g" \
		-e "s:%d\(.*cdr->billsec\):%ld\1:g" cdr_shell.c
}
