# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils subversion

## TODO:
#
# - Docs installation
# - more use flags
# - on-disk permission check + warning
#

DESCRIPTION=""
HOMEPAGE="http://www.asterisk.org/"
ESVN_REPO_URI="svn://svn.openpbx.org/openpbx/trunk"
ESVN_BOOTSTRAP="./bootstrap.sh"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE="alsa debug exosip fax jabber odbc osp oss mgr2 mysql postgresql pri profile speex t38 zap"

RDEPEND="virtual/libc
	dev-libs/newt
	sys-libs/ncurses
	sys-libs/zlib
	>=media-libs/spandsp-0.0.3_pre26
	osp? ( net-libs/osptoolkit )
	pri? ( net-libs/libpri )
	zap? ( net-misc/zaptel )
	alsa? ( media-libs/alsa-lib )
	mgr2? ( net-libs/libunicall )
	odbc? ( dev-db/unixODBC )
	mysql? ( dev-db/mysql )
	speex? ( media-libs/speex )
	exosip? ( >=net-libs/ortp-0.7.0
		>=net-libs/libosip-2.0.0
		net-libs/libeXosip )
	jabber? ( net-libs/loudmouth )
	postgres? ( dev-db/libpq )"

DEPEND="${RDEPEND}
	sys-devel/flex
	>=sys-devel/automake-1.9.6
	>=sys-devel/autoconf-2.59
	>=sys-devel/libtool-1.5.20"

S=${WORKDIR}/${PN}

src_unpack() {
	subversion_fetch
	cd "${S}"

	# to avoid conflict between the libedit in portage
	# and the one shipped with openpbx.org we have to tweak
	# configure.ac a little to be able to specify /usr/lib/openpbx.org
	# as libdir for all components...

	sed -i -e "s:\(opbxlibdir='\${libdir}\)/openpbx.org':\1':" \
		configure.ac || die "sed failed"

	if use profile || use debug; then
		find "${S}" -name "Makefile.am" -exec \
			sed -i -e "s:-fomit-frame-pointer::g" {} \;
	fi

	subversion_bootstrap
}

src_compile() {
	econf \
		--libdir=/usr/$(get_libdir)/openpbx.org	\
		--datadir=/var/lib			\
		--localstatedir=/var 			\
		--sharedstatedir=/var/lib/openpbx.org	\
		--with-directory-layout=lsb 		\
		`use_with speex codec_speex`		\
		`use_with jabber res_jabber`		\
		`use_with postgres cdr_pgsql`		\
		`use_with postgres res_config_pgsql`	\
		`use_with odbc res_odbc`		\
		`use_with odbc res_config_odbc`		\
		`use_with zap chan_zap`			\
		`use_with mgr2 chan_unicall`		\
		`use_with fax chan_fax`			\
		`use_with fax app_rxfax`		\
		`use_with fax app_txfax`		\
		`use_with t38 app_rxfax`		\
		`use_with t38 app_txfax`		\
		`use_with exosip chan_exosip`		\
		`use_enable debug`			\
		`use_enable profile`			\
		`use_enable t38`			\
		|| die "configure failed"

	emake || die "make failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "make install failed"

	dodoc README INSTALL AUTHORS COPYING NEWS BUGS
	dodoc TODO_FOR_AUTOMAKE SECURITY CREDITS HARDWARE LICENSE

	dodoc doc/README* doc/*.txt doc/*.pdf

	docinto samples
	dodoc "${D}"etc/openpbx.org/*.sample

	# remove dir
	rm -rf ${D}var/lib/openpbx.org/doc

	enewinitd "${FILESDIR}"/openpbx.rc6   openpbx
	enewconfd "${FILESDIR}"/openpbx.confd openpbx

	# don't delete these
	keepdir /var/{log,run,spool}/openpbx.org
	keepdir /var/lib/openpbx.org/{images,keys}
}

pkg_preinst() {
	if [[ -z "$(egetent passwd openpbx)" ]]; then
		einfo "Creating openpbx group and user..."
		enewgroup openpbx
		enewuser openpbx -1 -1 /var/lib/openpbx openpbx
	fi
}

pkg_postinst() {
	# only change permissions if openpbx wasn't installed before
	einfo "Fixing permissions..."

	chmod -R u=rwX,g=rX,o=	"${ROOT}"etc/openpbx.org
	chown -R root:openpbx   "${ROOT}"etc/openpbx.org

	for x in lib log run spool; do
		chmod -R u=rwX,g=rX,o=    "${ROOT}"var/${x}/openpbx.org
		chown -R openpbx:openpbx  "${ROOT}"var/${x}/openpbx.org
	done
}

pkg_config() {
	# TODO: ask user if he want to reset permissions back to sane defaults
	einfo "Do you want to reset the permissions and ownerships of openpbx.org to"
	einfo "the default values (y/N)?"
	read res

	res="$(echo $res | tr [[:upper:]] [[:lower:]])"

	if [[ "$res" = "y" ]] || \
	   [[ "$res" = "yes" ]]
	then
		einfo "First time installation, fixing permissions..."

		chmod -R u=rwX,g=rX,o=	"${ROOT}"etc/openpbx.org
		chown -R root:openpbx   "${ROOT}"etc/openpbx.org

		for x in lib log run spool; do
			chmod -R u=rwX,g=rX,o=    "${ROOT}"var/${x}/openpbx.org
			chown -R openpbx:openpbx  "${ROOT}"var/${x}/openpbx.org
		done
	fi
}
