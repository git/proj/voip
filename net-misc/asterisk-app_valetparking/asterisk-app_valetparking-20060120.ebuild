# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils toolchain-funcs

MY_PN="app_valetparking"

DESCRIPTION="Asterisk valetparking plugin"
HOMEPAGE="http://www.pbxfreeware.org/"
SRC_URI="http://www.netdomination.org/pub/asterisk/${P}.tar.bz2
	 mirror://gentoo/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"

DEPEND=">=net-misc/asterisk-1.2.0
	!<net-misc/asterisk-1.1.0"

S=${WORKDIR}/${MY_PN}

pkg_setup() {
	AST_MOD_SRC="${MY_PN}.c"
	AST_MOD_TARGET="${MY_PN}.so"
	AST_MOD_LIBS=""
	AST_MOD_CFLAGS="$(${ROOT}usr/bin/asterisk-config --cflags) $(${ROOT}usr/bin/asterisk-config --solink) -fPIC"
}

src_unpack() {
	unpack ${A}
	cd ${S}

	# "asterisk.h" -> <asterisk/asterisk.h>
	sed -i -e "s:\"asterisk\.h\":<asterisk/asterisk.h>:" \
		${AST_MOD_SRC}
}

src_compile() {
	# Build module
	einfo "Building ${AST_MOD_TARGET}..."
	$(tc-getCC) -o ${AST_MOD_TARGET} ${AST_MOD_CFLAGS} ${CFLAGS} \
		${AST_MOD_SRC} ${AST_MOD_LIBS} || die "Building of ${AST_MOD_TARGET} failed"
}

src_install() {
	einfo "Installing ${AST_MOD_TARGET}..."
	insinto /usr/$(get_libdir)/asterisk/modules
	doins ${AST_MOD_TARGET}	
}
