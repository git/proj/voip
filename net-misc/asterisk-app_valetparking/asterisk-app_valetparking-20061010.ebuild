# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-mod

MY_PN="app_valetparking"

DESCRIPTION="Asterisk Call Parking plugin"
HOMEPAGE="http://www.pbxfreeware.org/"
SRC_URI="http://r3mix.eu/pub/dist/asterisk/${P}.tar.bz2"

LICENSE="GPL-2"
KEYWORDS="~x86"

DEPEND=">=net-misc/asterisk-1.2.0"

S="${WORKDIR}/${AST_PN}"

src_unpack() {
	unpack ${A}
	cd "${S}"

	ast_fix_source app_valetparking.c
}
