# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils flag-o-matic

IUSE="ipv6 mysql radius postgres jabber ssl"

DESCRIPTION="OpenSIP Express Router"
HOMEPAGE="http://www.openser.org/"
SRC_URI="http://www.openser.org/pub/openser/${PV}/src/${P}-tls_src.tar.gz"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86"

RDEPEND=">=sys-devel/bison-1.35
	 >=sys-devel/flex-2.5.4a
	ssl? ( dev-libs/openssl )
	mysql? ( >=dev-db/mysql-3.23.52 )
	radius? ( >=net-dialup/radiusclient-ng-0.5.0 )
	postgres? ( dev-db/libpq )
	jabber? ( dev-libs/expat )"

DEPEND="${RDEPEND}"

S=${WORKDIR}/${P}-tls

src_unpack() {
	local modules

	# unpack ser source
	unpack ${A}
	cd ${S}

	# fix postgres module makefile
	epatch ${FILESDIR}/${P}-modpgsql-fix.diff

	use ipv6 || \
		sed -i -e "s/-DUSE_IPV6//g" Makefile.defs

	use ssl || \
		sed -i -e "s:^\(TLS=1\).*:#\1:" Makefile

	use mysql && \
		modules="${modules} mysql"

	use radius && \
		modules="${modules} auth_radius group_radius uri_radius avp_radius"

	use jabber && \
		modules="${modules} jabber"

	use postgres && \
		modules="${modules} postgres"

	# put list of modules into Makefile, we need the list
	# during compile and install phase...
	sed -i -e "s:^\(include_modules.*\):\1 ${modules} ${extmodules}:" \
		Makefile
}

src_compile() {
	# add -fPIC
	append-flags -fPIC

	make all \
		CFLAGS="${CFLAGS}" \
		cfg-prefix=/ \
		cfg-target=/etc/openser/ \
		${myconf} || die
}

src_install () {
	make install \
		prefix="" \
		bin-prefix=${D}/usr/sbin \
		bin-dir="" \
		cfg-prefix=${D}/etc \
		cfg-dir=openser/ \
		cfg-target=/etc/openser/ \
		modules-prefix=${D}/usr/lib/openser \
		modules-dir=modules \
		modules-target=/usr/lib/openser/modules/ \
		man-prefix=${D}/usr/share/man \
		man-dir="" \
		doc-prefix=${D}/usr/share/doc \
		doc-dir=${P} || die

	newinitd ${FILESDIR}/openser.rc6 openser
	newconfd ${FILESDIR}/openser.confd openser
	exeinto /usr/sbin
	newexe scripts/harv_ser.sh harv_openser.sh
	newexe scripts/sc openserctl
	newexe scripts/ser_mysql.sh openser_mysql.sh

	# fix manpages
	sed -i	-e "s:^.B /openser-${PV}AUTHORS:.B /usr/share/doc/${PF}/AUTHORS:" \
		-e "s:^.B /openser:.B /usr/sbin/openser:" \
		${D}/usr/share/man/*/*
}

pkg_preinst() {
	if [[ -z "$(egetent passwd openser)" ]]; then
		einfo "Adding openser user and group"
		enewgroup openser
		enewuser  openser -1 -1 /dev/null openser
	fi

	chown -R root:openser  ${D}/etc/openser
	chmod -R u=rwX,g=rX,o= ${D}/etc/openser
}
