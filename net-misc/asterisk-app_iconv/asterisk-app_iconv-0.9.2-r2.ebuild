# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-mod

DESCRIPTION="Asterisk application plugin for character conversion"
HOMEPAGE="http://www.mezzo.net/asterisk/"
SRC_URI="http://www.mezzo.net/asterisk/${AST_P}.tgz"

LICENSE="GPL-2"
KEYWORDS="~ppc ~x86"

# depends on iconv support
DEPEND="sys-libs/glibc
	>=net-misc/asterisk-1.0.5-r2"

src_unpack() {
	unpack ${A}
	cd "${S}"

	# patch makefile
	sed -i -e "s:^\(RES=\).*echo \"\(.*\)\".*:\1\2:g" \
		-e "s:^\(CFLAGS\)=:\1+=:g" Makefile
}

src_compile() {
	emake CC=$(tc-getCC) SOLINK="${AST_LDFLAGS} ${LDFLAGS}" || die "emake failed"
}
