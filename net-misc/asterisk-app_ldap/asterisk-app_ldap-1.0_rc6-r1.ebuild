# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit asterisk-mod

MY_P="${AST_PN}-${PV/_}"

DESCRIPTION="Asterisk application plugin to do lookups in a LDAP directory"
HOMEPAGE="http://www.mezzo.net/asterisk/"
SRC_URI="http://www.mezzo.net/asterisk/${MY_P}.tgz"

LICENSE="GPL-2"
KEYWORDS="~x86 ~ppc"

# depends on iconv support
DEPEND="sys-libs/glibc
	>=net-nds/openldap-2.0.0
	>=net-misc/asterisk-1.0.7-r1"

S="${WORKDIR}/${MY_P}"

src_unpack() {
	unpack ${A}
	cd "${S}"

	# rename sample config
	mv ldap.conf.sample ldap.conf

	# patch makefile
	sed -i -e "s:^\(RES=\).*echo \"\(.*\)\".*:\1\2:g" \
		-e "s:^\(CFLAGS\)=\(.*\):\1+=\2 -DLDAP_DEPRECATED:g" Makefile
}

src_compile() {
	emake CC=$(tc-getCC) SOLINK="${AST_LDFLAGS} ${LDFLAGS}" || die "emake failed"
}
