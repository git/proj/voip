# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="GNU Bayonne2"
HOMEPAGE="http://www.gnu.org/software/bayonne/"
SRC_URI="mirror://gnu/bayonne/${P}.tar.gz"

KEYWORDS="~x86"
LICENSE="GPL-2"
#IUSE="h323 linguas_de linguas_en linguas_it linguas_fr linguas_ru lingues_es"
IUSE=""
SLOT="0"

RDEPEND=">=dev-cpp/commoncpp2-1.5.0
	 >=dev-libs/ccscript3-1.1.0
	 >=media-libs/ccaudio2-0.9.5
	 >=net-libs/ccrtp-1.5.0
	 >=net-libs/libeXosip-2.2.2"
#	 h323? ( >=dev-libs/pwlib-1.8.4	
#		 >=net-libs/openh323-1.15.3 )"

# TODO patch for optional usage of net-libs/libeXosip

DEPEND="${RDEPEND}"

src_compile() {
	local myconf linguas=""
#
#	for x in de en es fr it ru ; do
#		use linguas_$x &&
#			linguas="$x ${linguas}"
#	done
#
#	[[ -n "${linguas}" ]] \
#		&& myconf="--enable-voices --with-voices=$(echo "${linguas}" | sed -e "s: $::" | tr ' ' ',')"

	# language selection doesn't work, build all voices
	myconf="--enable-voices"

	econf \
		${myconf} || die "configure failed"
# h323 fails to build
#		$(use_enable h323 openh323) \
#		$(use_with h323 openh323 /usr) \
#		$(use_with h323 pwlib /usr) \

	emake -j1 || die "emake failed"
}

src_install() {
	dodir /etc/init.d
	dodir /etc/bayonne

	emake DESTDIR=${D} install || die "emake install failed"
	dodoc README ChangeLog AUTHORS FAQ NEWS THANKS TODO

	keepdir /var/lib/bayonne/{apps,admin,html,php}

	insinto /etc/bayonne
	doins config/*.conf

	newinitd ${FILESDIR}/bayonne.rc6 bayonne
	newconfd ${FILESDIR}/bayonne.confd bayonne
	fowners -R root:bayonne	/etc/bayonne
	fowners -R bayonne:bayonne /var/lib/bayonne
	fperms -R u=rwX,g=rX,o=	/etc/bayonne /var/lib/bayonne
}

pkg_preinst() {
	if [[ -z "$(egetent passwd bayonne)" ]]; then
		einfo "Creating bayonne user and group"
		enewgroup bayonne
		enewuser bayonne -1 -1 /var/lib/bayonne bayonne
	fi
}
