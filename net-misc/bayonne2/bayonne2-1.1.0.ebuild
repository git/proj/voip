# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="GNU Bayonne2"
HOMEPAGE="http://www.gnu.org/software/bayonne/"
SRC_URI="mirror://gnu/bayonne/${P}.tar.gz"

KEYWORDS="~x86"
LICENSE="GPL-2"
#IUSE="h323 linguas_de linguas_en linguas_it linguas_fr linguas_ru lingues_es"
IUSE=""
SLOT="0"

RDEPEND=">=dev-cpp/commoncpp2-1.3.0
	 >=dev-libs/ccscript3-1.0.1
	 >=media-libs/ccaudio2-0.7.1
	 >=net-libs/ccrtp-1.3.4"
#	 h323? ( >=dev-libs/pwlib-1.8.4	
#		 >=net-libs/openh323-1.15.3 )"

DEPEND="${RDEPEND}"

src_compile() {
	local myconf linguas=""
#
#	for x in de en es fr it ru ; do
#		use linguas_$x &&
#			linguas="$x ${linguas}"
#	done
#
#	[[ -n "${linguas}" ]] \
#		&& myconf="--enable-voices --with-voices=$(echo "${linguas}" | sed -e "s: $::" | tr ' ' ',')"

	# language selection doesn't work, build all voices
	myconf="--enable-voices"

	econf \
		${myconf} || die "configure failed"
# h323 fails to build
#		$(use_enable h323 openh323) \
#		$(use_with h323 openh323 /usr) \
#		$(use_with h323 pwlib /usr) \

	emake || die "make failed"
}

src_install() {
	dodir /etc/init.d
	dodir /etc/bayonne

	make DESTDIR=${D} install || die
	dodoc README ChangeLog INSTALL AUTHORS COPYING FAQ NEWS THANKS TODO

	keepdir /var/lib/bayonne/{apps,admin,html,php}

	insinto /etc/bayonne
	doins config/*.conf

	newinitd ${FILESDIR}/bayonne.rc6 bayonne
	newconfd ${FILESDIR}/bayonne.confd bayonne
}

pkg_preinst() {
	if [[ -z "$(egetent passwd bayonne)" ]]; then
		einfo "Creating bayonne user and group"
		enewgroup bayonne
		enewuser bayonne -1 -1 /var/lib/bayonne bayonne
	fi
}

pkg_postinst() {
	# set permissions
	chown -R root:bayonne	${ROOT}etc/bayonne
	chmod -R u=rwX,g=rX,o=	${ROOT}etc/bayonne

	chown -R bayonne:bayonne ${ROOT}var/lib/bayonne
	chmod -R u=rwX,g=rX,o=	 ${ROOT}var/lib/bayonne
}
