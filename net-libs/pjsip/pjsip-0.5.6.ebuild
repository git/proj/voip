# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils toolchain-funcs flag-o-matic

MY_PN="pjproject"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="PJSIP"
HOMEPAGE="http://www.pjsip.org/"
SRC_URI="http://www.pjsip.org/release/${PV}/pjproject-${PV}.tar.gz"

KEYWORDS="~x86"
SLOT="0"
LICENSE="GPL-2"
IUSE="doc"

DEPEND="doc? ( app-doc/doxygen )"
RDEPEND=""

S=${WORKDIR}/${MY_P}

src_unpack() {
	unpack ${A}
	cd "${S}"

	# change arch detection code to always use "uname -m"
	#epatch "${FILESDIR}"/${PN}-0.5.5.1-archdetect.diff

	# only run doxygen in a docs subdirectory if there's a doxygen.cfg file
	epatch "${FILESDIR}"/${PN}-0.5.5.1-generate-docs.diff

	# a subdir have been renamed and another one added, update doxygen.cfg
	#epatch "${FILESDIR}"/${PN}-0.5.5.1-pjsip-doxygen.cfg.diff

	# don't add stun.o multiple times to libpjlib-util.a
	epatch "${FILESDIR}"/${PN}-0.5.5.1-remove-duplicate-stun.o.diff
}

src_compile() {
	local arch="$(tc-arch-kernel)"

	# catch some rare exceptions
	case "${arch}" in
		ppc)
			# still ppc on 32bit pre-2.6.16 machines
			arch="powerpc"
			;;
	esac

	append-flags -fPIC

	MACHINE="${arch}" econf || die "econf failed"
	emake || die "emake failed"

	if use doc; then
		emake doc || die "building of pjsip docs failed"
	fi
}

src_install() {
	local arch="$(tc-arch-kernel)"

	# catch some rare exceptions
	case "${arch}" in
		ppc)
			# still ppc on 32bit pre-2.6.16 machines
			arch="powerpc"
			;;
	esac

	# install libraries
	insinto /usr/$(get_libdir)
	for x in `find "${S}" -name "*.a"`; do
		newins "${x}" "$(basename ${x//-${arch}-*/.a})"

		# experimental, create shared lib from static
		soname="$(basename ${x//-${arch}-*.a/.so})"
		$(tc-getCC) -shared -o "${D}/usr/$(get_libdir)/${soname}" \
			-Wl,-soname,${soname} -Wl,-whole-archive ${x} -Wl,-no-whole-archive -lpthread -lc
	done

	# install header
	dodir /usr/include
	for x in pjsip pjlib pjlib-util pjmedia; do
		cp -Rf "${x}"/include/* "${D}"/usr/include
	done

	# install sample apps
	newbin pjsip-apps/bin/pjsua-${arch}-* pjsua
	for x in `ls pjsip-apps/bin/samples/*-${arch}-*`; do
		newbin "${x}" "pjsip_$(basename ${x//-${arch}-*/})"
	done

	# docs
	dodoc COPYING INSTALL.txt README.txt README-RTEMS

	for x in pjsip pjlib pjlib-util pjmedia; do
		docinto "${x}"
		dodoc "${x}"/docs/*.{pdf,txt} 2>/dev/null

		if use doc && [[ -d "${x}"/docs/html ]]; then
			cp -Rf "${x}/docs/html" "${D}/usr/share/doc/${PF}/${x}"
		fi
	done
}
