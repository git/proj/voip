# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils flag-o-matic

DESCRIPTION="A SIP stack supporting many SIP extensions/features."
HOMEPAGE="http://www.pjsip.org/"
SRC_URI="http://www.pjsip.org/release/${PV}/pjproject-${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="1"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="sound ssl speex ilbc gsm examples python"

RDEPEND="sound? ( media-libs/alsa-lib )
	ssl? ( dev-libs/openssl )
	python? ( dev-lang/python )"
DEPEND="${RDEPEND}"

S="${WORKDIR}/pjproject-${PV}"

src_unpack() {
	unpack ${A}

	cd "${S}"

	mv -f aconfigure.ac configure.ac
	mv -f aconfigure configure

	epatch "${FILESDIR}/${P}-saneMake.diff"
}

src_compile() {
	econf $(use_enable sound) \
		$(use_enable ssl) \
		$(use_enable speex) \
		$(use_enable speex speex-aec) \
		$(use_enable ilbc) \
		$(use_enable gsm) || die "econf failed."
	emake -j1 || die "emake failed."
}

src_install() {
	DESTDIR="${D}" emake install || die "emake install failed."

	if use python; then
		pushd pjsip-apps/src/py_pjsua
		python setup.py install --prefix="${D}/usr/"
		popd
	fi

	dodoc COPYING README.txt README-RTEMS INSTALL.txt
	if use examples; then
		insinto /usr/share/doc/${P}/examples
		doins ${S}/pjsip-apps/src/samples/*
	fi
}

